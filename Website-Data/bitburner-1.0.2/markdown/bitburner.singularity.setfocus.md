<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [bitburner](./bitburner.md) &gt; [Singularity](./bitburner.singularity.md) &gt; [setFocus](./bitburner.singularity.setfocus.md)

## Singularity.setFocus() method

Set the players focus.

<b>Signature:</b>

```typescript
setFocus(focus: boolean): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  focus | boolean |  |

<b>Returns:</b>

void

## Remarks

RAM cost: 0.1 GB

Singularity - Level 2

